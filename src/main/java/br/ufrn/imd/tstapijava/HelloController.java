package br.ufrn.imd.tstapijava;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;

@RestController
public class HelloController {

    private static Instant STARTUP = Instant.now();
    private static String VERSION = "1.0.1";


    @GetMapping("/hello")
    public HelloDTO hello(){
        HelloDTO hello = new HelloDTO(VERSION, STARTUP.toString(), Instant.now().toString());
        return hello;
    }

    @GetMapping("/version")
    public String version(){
        return "1.0.1";
    }

}
