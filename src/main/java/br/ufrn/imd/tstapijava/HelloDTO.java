package br.ufrn.imd.tstapijava;

public record HelloDTO(String version, String startup, String now) {
}
