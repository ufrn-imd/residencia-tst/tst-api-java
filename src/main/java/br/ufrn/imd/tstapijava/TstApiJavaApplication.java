package br.ufrn.imd.tstapijava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TstApiJavaApplication {
    public static void main(String[] args) {
        SpringApplication.run(TstApiJavaApplication.class, args);
    }

}
